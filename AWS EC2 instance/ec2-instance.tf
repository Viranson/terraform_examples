##Amazon EC2 Instance
resource "aws_instance" "citadel" {
  ami           = var.ami # us-west-2
  instance_type = var.instance_type
  key_name      = aws_key_pair.citadel-key.key_name
  user_data     = file("/root/terraform-challenges/project-citadel/install-nginx.sh")
}