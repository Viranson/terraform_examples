
##Elastic IP
resource "aws_eip" "eip" {
  instance = aws_instance.citadel.id
  vpc      = true
  ##Print aws_eip public_dns attribute to local file
  provisioner "local-exec" {
    command = "echo ${self.public_dns} >> /root/citadel_public_dns.txt"
  }
}