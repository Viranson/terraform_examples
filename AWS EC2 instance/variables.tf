variable "region" {
  type        = string
  description = "AWS Region"
}

variable "ami" {
  type        = string
  description = "Amazon Machine Image ID to use for EC2 instance"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "Amazon EC2 instance type"
}