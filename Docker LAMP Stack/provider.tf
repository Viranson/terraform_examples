##Download Terraform 1.1.5 binary file
##wget https://releases.hashicorp.com/terraform/1.1.5/terraform_1.1.5_linux_amd64.zip
terraform {
  required_providers {
    ##https://registry.terraform.io/providers/kreuzwerker/docker/2.16.0
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.16.0"
    }
  }
}

provider "docker" {}