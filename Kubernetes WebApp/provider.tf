##Download Terraform 1.1.5 binary file
##wget https://releases.hashicorp.com/terraform/1.1.5/terraform_1.1.5_linux_amd64.zip && unzip terraform_1.1.5_linux_amd64.zip && rm terraform_1.1.5_linux_amd64.zip && mv terraform /usr/local/bin && terraform --version
terraform {
  required_providers {
    ##https://registry.terraform.io/providers/hashicorp/kubernetes/2.11.0
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.11.0"
    }
  }
}

provider "kubernetes" {
  # Configuration options
  config_path = "/root/.kube/config"
}