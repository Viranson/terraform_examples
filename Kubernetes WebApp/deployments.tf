##FRONTEND DEPLOYMENT
##https://registry.terraform.io/providers/hashicorp/kubernetes/2.11.0/docs/resources/deployment
resource "kubernetes_deployment" "frontend" {
  metadata {
    name = "frontend"
    labels = {
      name = "frontend"
    }
  }

  spec {
    replicas = 4

    selector {
      match_labels = {
        name = "webapp"
      }
    }

    template {
      metadata {
        labels = {
          name = "webapp"
        }
      }

      spec {
        container {
          image = "kodekloud/webapp-color:v1"
          name  = "simple-webapp"
          port {
            container_port = 8080
          }

          #   resources {
          #     limits = {
          #       cpu    = "0.5"
          #       memory = "512Mi"
          #     }
          #     requests = {
          #       cpu    = "250m"
          #       memory = "50Mi"
          #     }
          #   }

          liveness_probe {
            http_get {
              path = "/"
              port = 80
            }

            #       http_header {
            #         name  = "X-Custom-Header"
            #         value = "Awesome"
            #       }
            #     }

            # initial_delay_seconds = 3
            # period_seconds        = 3
          }
        }
      }
    }
  }
}