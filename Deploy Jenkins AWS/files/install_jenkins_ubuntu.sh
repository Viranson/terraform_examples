#!/bin/bash
##Script to install Jenkins on Ubuntu|Debian

sudo apt update

sudo apt install -y default-jre
#sudo apt install -y openjkd-11-jre-headless

java --version

#wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

#sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null

echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null

sudo add-apt-repository universe -y

sudo apt update -y

sudo apt install -y jenkins

sudo systemctl enable jenkins

sudo systemctl start jenkins

#sudo ufw allow 8080
#sudo ufw enable

sudo cat /var/lib/jenkins/secrets/initialAdminPassword