##Create Amazon EC2 Key Pair from local public key file
resource "aws_key_pair" "jenkins-key" {
  key_name   = "jenkins"
  public_key = file("files/ec2-jenkins-key.pub")
}
