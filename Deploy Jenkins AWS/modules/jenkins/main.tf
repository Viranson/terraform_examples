data "aws_default_vpc" "jenkins-default-vpc" {
  tags = {
    Name = "Jenkins VPC"
  }
}

resource "aws_security_group" "allow_ssh_http_https_jenkins8080" {
  name        = "var.security_group_name"
  description = "Allow SSH, HTTP, HTTPS and jenkins 8080 inbound traffic"
  vpc_id = aws_default_vpc.jenkins-default-vpc.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "Jenkins 8080 from VPC"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    description      = "ALL to VPC"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh_http_https_jenkins8080"
  }
}

resource "aws_instance" "jenkins-srv" {
  ami             = "ami-00de6c6491fdd3ef5"
  instance_type   = var.instancetype
  key_name        = "devops-jenkins" #Exixting key pair
  user_data     = file("../../files/install_jenkins_ubuntu.sh") #Insérer le ecript comme user-data pour installer jenkins au démarrage ou copier le script sur le serveur et exécuter le remote-exec
  tags            = var.aws_common_tag
  security_groups = ["${aws_security_group.allow_ssh_http_https_jenkins8080.id}"]

# Utiliser ce bloc si vous préférez utiliser le remote-exec
  # provisioner "file" {
  #   source = "../../files/install_jenkins_ubuntu.sh"
  #   destination = "/tmp/install_jenkins.sh"
  # }
  # provisioner "remote-exec" {
  #   inline = [
  #     "sudo chmod + x /tmp/install_jenkins_ubuntu.sh",
  #     "sh /tmp/install_jenkins.sh"
  #   ]

  #   connection {
  #     type        = "ssh"
  #     user        = "ec2-user"
  #     private_key = file("../../files/jenkins-srv.pem")
  #     host        = self.public_ip
  #   }
  # }

  # # #Pour forcer la suppression des blocs ebs créés pour les instances
  # # root_block_device {
  # #   delete_on_termination = true
  # # }
}

resource "aws_eip" "jenkins-srv_eip" {
  instance = "$(aws_instance.jenkins-srv.id)"
  vpc      = true

  provisioner "local-exec" {
    command = "echo PUBLIC_IP: ${aws_eip.jenkins-srv_eip.public_ip} ; ID: ${aws_instance.jenkins-srv.id} ; AZ: ${aws_instance.jenkins-srv.availability_zone}; > infos_jenkins-srv.txt"
  }
}
