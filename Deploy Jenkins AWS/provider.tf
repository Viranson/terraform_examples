terraform {
  required_providers {
    ##https://registry.terraform.io/providers/kreuzwerker/docker/2.16.0
    aws = {
      source  = "hashicorp/aws"
      version = "4.15.0"
    }
  }
}

provider "aws" {
  region = var.region
  #   skip_credentials_validation = true
  #   skip_requesting_account_id  = true
  #   access_key = var.access_key
  #   secret_key = var.secret_key

  ##Set endpoints because using LocalStack to emulate AWS Cloud Services with endpoint http://aws:4566 (localstack)
  #   endpoints {
  #     ec2            = "http://aws:4566"
  #     apigateway     = "http://aws:4566"
  #     cloudformation = "http://aws:4566"
  #     cloudwatch     = "http://aws:4566"
  #     dynamodb       = "http://aws:4566"
  #     es             = "http://aws:4566"
  #     firehose       = "http://aws:4566"
  #     iam            = "http://aws:4566"
  #     kinesis        = "http://aws:4566"
  #     lambda         = "http://aws:4566"
  #     route53        = "http://aws:4566"
  #     redshift       = "http://aws:4566"
  #     s3             = "http://aws:4566"
  #     secretsmanager = "http://aws:4566"
  #     ses            = "http://aws:4566"
  #     sns            = "http://aws:4566"
  #     sqs            = "http://aws:4566"
  #     ssm            = "http://aws:4566"
  #     stepfunctions  = "http://aws:4566"
  #     sts            = "http://aws:4566"
  #   }
}